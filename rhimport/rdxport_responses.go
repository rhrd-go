//
//  rhimportd
//
//  The Radio Helsinki Rivendell Import Daemon
//
//
//  Copyright (C) 2015-2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of rhimportd.
//
//  rhimportd is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  rhimportd is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with rhimportd. If not, see <http://www.gnu.org/licenses/>.
//

package rhimport

import (
	"encoding/xml"
	"fmt"
	"io"
)

type rdWebResult struct {
	ResponseCode      int    `xml:"ResponseCode"`
	ErrorString       string `xml:"ErrorString"`
	AudioConvertError int    `xml:"AudioConvertError"`
}

func newRDWebResultFromXML(data io.Reader) (res *rdWebResult, err error) {
	decoder := xml.NewDecoder(data)
	res = &rdWebResult{}
	if xmlerr := decoder.Decode(res); xmlerr != nil {
		err = fmt.Errorf("Error parsing XML response: %s", xmlerr)
		return
	}
	return
}

type rdCartAdd struct {
	Carts []rdCart `xml:"cart"`
}

type rdCart struct {
	Number             uint   `xml:"number"`
	Type               string `xml:"type"`
	GroupName          string `xml:"groupName"`
	Title              string `xml:"title"`
	Artist             string `xml:"artist"`
	Album              string `xml:"album"`
	Year               string `xml:"year"`
	Label              string `xml:"label"`
	Client             string `xml:"client"`
	Agency             string `xml:"agency"`
	Publisher          string `xml:"publisher"`
	Composer           string `xml:"composer"`
	UserDefined        string `xml:"userDefined"`
	UsageCode          uint   `xml:"usageCode"`
	ForcedLength       string `xml:"forcedLength"`
	AverageLength      string `xml:"averageLength"`
	LengthDeviation    string `xml:"lengthDeviation"`
	AverageSegueLength string `xml:"averageSegueLenth"`
	AverageHookLength  string `xml:"averageHookLength"`
	CutQuantity        uint   `xml:"cutQuantity"`
	LastCutPlayed      uint   `xml:"lastCutPlayed"`
	Validity           uint   `xml:"validity"`
	EnforceLength      bool   `xml:"enforceLength"`
	Asynchronous       bool   `xml:"asyncronous"`
	Owner              string `xml:"owner"`
	MetadataDatetime   string `xml:"metadataDatetime"`
}

func newRDCartAddFromXML(data io.Reader) (cart *rdCartAdd, err error) {
	decoder := xml.NewDecoder(data)
	cart = &rdCartAdd{}
	if xmlerr := decoder.Decode(cart); xmlerr != nil {
		err = fmt.Errorf("Error parsing XML response: %s", xmlerr)
		return
	}
	return
}

type rdCutAdd struct {
	Cuts []rdCut `xml:"cut"`
}

type rdCut struct {
	Name             string `xml:"cutName"`
	CartNumber       uint   `xml:"cartNumber"`
	Number           uint   `xml:"cutNumber"`
	EverGreen        bool   `xml:"evergreen"`
	Description      string `xml:"description"`
	OutCue           string `xml:"outcue"`
	ISRC             string `xml:"isrc"`
	ISCI             string `xml:"isci"`
	Length           int    `xml:"length"`
	OriginDateTime   string `xml:"originDatetime"`
	StartDateTime    string `xml:"startDatetime"`
	EndDateTime      string `xml:"endDatetime"`
	Sunday           bool   `xml:"sun"`
	Monday           bool   `xml:"mon"`
	Tuesday          bool   `xml:"tue"`
	Wednesday        bool   `xml:"wed"`
	Thursday         bool   `xml:"thu"`
	Friday           bool   `xml:"fri"`
	Saturday         bool   `xml:"sat"`
	StartDaypart     string `xml:"startDaypart"`
	EndDayPart       string `xml:"endDaypart"`
	OriginName       string `xml:"originName"`
	Weight           int    `xml:"weight"`
	LastPlayDateTime string `xml:"lastPlayDatetime"`
	PlayCounter      uint   `xml:"playCounter"`
	LocalCounter     uint   `xml:"localCounter"`
	Validiy          uint   `xml:"validity"`
	CondingFormat    int    `xml:"codingFormat"`
	SampleRate       int    `xml:"sampleRate"`
	BitRate          int    `xml:"bitRate"`
	Channels         int    `xml:"channels"`
	PlayGain         int    `xml:"playGain"`
	StartPoint       int    `xml:"startPoint"`
	EndPoint         int    `xml:"endPoint"`
	FadeUpPoint      int    `xml:"fadeupPoint"`
	FadeDownPoint    int    `xml:"fadedownPoint"`
	SegueStartPoint  int    `xml:"segueStartPoint"`
	SegueEndPoint    int    `xml:"segueEndPoint"`
	SegueGain        int    `xml:"segueGain"`
	HookStartPoint   int    `xml:"hookStartPoint"`
	HookEndPoint     int    `xml:"hookEndPoint"`
	TalkStartPoint   int    `xml:"talkStartPoint"`
	TalkEndPoint     int    `xml:"talkEndPoint"`
}

func newRDCutAddFromXML(data io.Reader) (cut *rdCutAdd, err error) {
	decoder := xml.NewDecoder(data)
	cut = &rdCutAdd{}
	if xmlerr := decoder.Decode(cut); xmlerr != nil {
		err = fmt.Errorf("Error parsing XML response: %s", xmlerr)
		return
	}
	return
}
