//
//  rhimportd
//
//  The Radio Helsinki Rivendell Import Daemon
//
//
//  Copyright (C) 2015-2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of rhimportd.
//
//  rhimportd is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  rhimportd is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with rhimportd. If not, see <http://www.gnu.org/licenses/>.
//

package rhimport

import (
	"encoding/xml"
	"fmt"
	"io"
)

type bs1770Result struct {
	Album bs1770ResultAlbum `xml:"album"`
}

type bs1770ResultAlbum struct {
	Tracks  []bs1770ResultTrack `xml:"track"`
	Summary bs1770ResultSummary `xml:"summary"`
}

type bs1770ResultTrack struct {
	Total            uint                  `xml:"total,attr"`
	Number           uint                  `xml:"number,attr"`
	File             string                `xml:"file,attr"`
	Integrated       bs1770ResultValueLUFS `xml:"integrated"`
	Momentary        bs1770ResultValueLUFS `xml:"momentary"`
	ShorttermMaximum bs1770ResultValueLUFS `xml:"shortterm-maximum"`
	SamplePeak       bs1770ResultValueSPFS `xml:"sample-peak"`
	TruePeak         bs1770ResultValueTPFS `xml:"true-peak"`
}

type bs1770ResultSummary struct {
	Total            uint                  `xml:"total,attr"`
	Integrated       bs1770ResultValueLUFS `xml:"integrated"`
	Momentary        bs1770ResultValueLUFS `xml:"momentary"`
	ShorttermMaximum bs1770ResultValueLUFS `xml:"shortterm-maximum"`
	SamplePeak       bs1770ResultValueSPFS `xml:"sample-peak"`
	TruePeak         bs1770ResultValueTPFS `xml:"true-peak"`
}

type bs1770ResultValueLUFS struct {
	LUFS float64 `xml:"lufs,attr"`
	LU   float64 `xml:"lu,attr"`
}

type bs1770ResultValueRange struct {
	LUFS float64 `xml:"lufs,attr"`
}

type bs1770ResultValueSPFS struct {
	SPFS   float64 `xml:"spfs,attr"`
	Factor float64 `xml:"factor,attr"`
}

type bs1770ResultValueTPFS struct {
	TPFS   float64 `xml:"tpfs,attr"`
	Factor float64 `xml:"factor,attr"`
}

func newBS1770ResultFromXML(data io.Reader) (res *bs1770Result, err error) {
	decoder := xml.NewDecoder(data)
	res = &bs1770Result{}
	if xmlerr := decoder.Decode(res); xmlerr != nil {
		err = fmt.Errorf("Error parsing XML response: %s", xmlerr)
		return
	}
	return
}
