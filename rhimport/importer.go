//
//  rhimportd
//
//  The Radio Helsinki Rivendell Import Daemon
//
//
//  Copyright (C) 2015-2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of rhimportd.
//
//  rhimportd is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  rhimportd is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with rhimportd. If not, see <http://www.gnu.org/licenses/>.
//

package rhimport

import (
	"bufio"
	"bytes"
	"fmt"
	"mime/multipart"
	"net/http"

	"github.com/andelf/go-curl"
)

func (res *Result) fromRDWebResult(rdres *rdWebResult) {
	res.ResponseCode = rdres.ResponseCode
	res.ErrorString = rdres.ErrorString
	if rdres.AudioConvertError != 0 {
		res.ErrorString += fmt.Sprintf(", Audio Convert Error: %d", rdres.AudioConvertError)
	}
}

//****************************************************************
//**   managing carts and cuts

func addCart(ctx *Context, res *Result) (err error) {
	ctx.dbglog.Printf("importer: addCart() called for cart: %d", ctx.Cart)

	if ctx.GroupName == "" {
		if err = ctx.getGroupOfCart(); err != nil {
			return
		}
	}

	var b bytes.Buffer
	w := multipart.NewWriter(&b)

	if err = w.WriteField("COMMAND", "12"); err != nil {
		return
	}
	if err = w.WriteField("LOGIN_NAME", ctx.UserName); err != nil {
		return
	}
	if err = w.WriteField("PASSWORD", ctx.Password); err != nil {
		return
	}
	if err = w.WriteField("GROUP_NAME", ctx.GroupName); err != nil {
		return
	}
	if err = w.WriteField("TYPE", "audio"); err != nil {
		return
	}
	if ctx.Cart != 0 {
		if err = w.WriteField("CART_NUMBER", fmt.Sprintf("%d", ctx.Cart)); err != nil {
			return
		}
	}
	w.Close()

	var resp *http.Response
	if resp, err = sendPostRequest(ctx.conf.RDXportEndpoint, &b, w.FormDataContentType()); err != nil {
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		var rdres *rdWebResult
		if rdres, err = newRDWebResultFromXML(resp.Body); err != nil {
			return
		}
		res.fromRDWebResult(rdres)
		res.Cart = ctx.Cart
		return
	}
	var cartadd *rdCartAdd
	if cartadd, err = newRDCartAddFromXML(resp.Body); err != nil {
		return
	}
	res.ResponseCode = resp.StatusCode
	res.ErrorString = "OK"
	res.Cart = cartadd.Carts[0].Number
	ctx.Cart = res.Cart
	return
}

func addCut(ctx *Context, res *Result) (err error) {
	ctx.dbglog.Printf("importer: addCut() called for cart/cut: %d/%d", ctx.Cart, ctx.Cut)
	var b bytes.Buffer
	w := multipart.NewWriter(&b)

	if err = w.WriteField("COMMAND", "10"); err != nil {
		return
	}
	if err = w.WriteField("LOGIN_NAME", ctx.UserName); err != nil {
		return
	}
	if err = w.WriteField("PASSWORD", ctx.Password); err != nil {
		return
	}
	if err = w.WriteField("CART_NUMBER", fmt.Sprintf("%d", ctx.Cart)); err != nil {
		return
	}
	w.Close()

	var resp *http.Response
	if resp, err = sendPostRequest(ctx.conf.RDXportEndpoint, &b, w.FormDataContentType()); err != nil {
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		var rdres *rdWebResult
		if rdres, err = newRDWebResultFromXML(resp.Body); err != nil {
			return
		}
		res.fromRDWebResult(rdres)
		res.Cart = ctx.Cart
		res.Cut = ctx.Cut
		return
	}
	var cutadd *rdCutAdd
	if cutadd, err = newRDCutAddFromXML(resp.Body); err != nil {
		return
	}
	res.ResponseCode = resp.StatusCode
	res.ErrorString = "OK"
	res.Cart = ctx.Cart
	res.Cut = cutadd.Cuts[0].Number
	ctx.Cut = cutadd.Cuts[0].Number
	return
}

func removeCart(ctx *Context, res *Result) (err error) {
	ctx.dbglog.Printf("importer: removeCart() called for cart: %d", ctx.Cart)
	var b bytes.Buffer
	w := multipart.NewWriter(&b)

	if err = w.WriteField("COMMAND", "13"); err != nil {
		return
	}
	if err = w.WriteField("LOGIN_NAME", ctx.UserName); err != nil {
		return
	}
	if err = w.WriteField("PASSWORD", ctx.Password); err != nil {
		return
	}
	if err = w.WriteField("CART_NUMBER", fmt.Sprintf("%d", ctx.Cart)); err != nil {
		return
	}
	w.Close()

	var resp *http.Response
	if resp, err = sendPostRequest(ctx.conf.RDXportEndpoint, &b, w.FormDataContentType()); err != nil {
		return
	}
	defer resp.Body.Close()

	var rdres *rdWebResult
	if rdres, err = newRDWebResultFromXML(resp.Body); err != nil {
		return
	}
	res.fromRDWebResult(rdres)
	res.Cart = ctx.Cart
	return
}

func removeCut(ctx *Context, res *Result) (err error) {
	ctx.dbglog.Printf("importer: removeCut() called for cart/cut: %d/%d", ctx.Cart, ctx.Cut)
	var b bytes.Buffer
	w := multipart.NewWriter(&b)

	if err = w.WriteField("COMMAND", "11"); err != nil {
		return
	}
	if err = w.WriteField("LOGIN_NAME", ctx.UserName); err != nil {
		return
	}
	if err = w.WriteField("PASSWORD", ctx.Password); err != nil {
		return
	}
	if err = w.WriteField("CART_NUMBER", fmt.Sprintf("%d", ctx.Cart)); err != nil {
		return
	}
	if err = w.WriteField("CUT_NUMBER", fmt.Sprintf("%d", ctx.Cut)); err != nil {
		return
	}
	w.Close()

	var resp *http.Response
	if resp, err = sendPostRequest(ctx.conf.RDXportEndpoint, &b, w.FormDataContentType()); err != nil {
		return
	}
	defer resp.Body.Close()

	var rdres *rdWebResult
	if rdres, err = newRDWebResultFromXML(resp.Body); err != nil {
		return
	}
	res.fromRDWebResult(rdres)
	res.Cart = ctx.Cart
	res.Cut = ctx.Cut
	return
}

func sendPostRequest(url string, b *bytes.Buffer, contenttype string) (resp *http.Response, err error) {
	var req *http.Request
	if req, err = http.NewRequest("POST", url, b); err != nil {
		return
	}
	if contenttype != "" {
		req.Header.Set("Content-Type", contenttype)
	}

	client := &http.Client{}
	if resp, err = client.Do(req); err != nil {
		return
	}
	return
}

//****************************************************************
//**   import audio

func importAudioCreateRequest(ctx *Context, easy *curl.CURL) (form *curl.Form, err error) {
	form = curl.NewForm()

	if err = form.Add("COMMAND", "2"); err != nil {
		return
	}
	if err = form.Add("LOGIN_NAME", ctx.UserName); err != nil {
		return
	}
	if err = form.Add("PASSWORD", ctx.Password); err != nil {
		return
	}
	if err = form.Add("CART_NUMBER", fmt.Sprintf("%d", ctx.Cart)); err != nil {
		return
	}
	if err = form.Add("CUT_NUMBER", fmt.Sprintf("%d", ctx.Cut)); err != nil {
		return
	}
	if err = form.Add("CHANNELS", fmt.Sprintf("%d", ctx.Channels)); err != nil {
		return
	}
	if err = form.Add("NORMALIZATION_LEVEL", fmt.Sprintf("%d", ctx.NormalizationLevel)); err != nil {
		return
	}
	if err = form.Add("AUTOTRIM_LEVEL", fmt.Sprintf("%d", ctx.AutotrimLevel)); err != nil {
		return
	}
	if err = form.Add("USE_METADATA", bool2str[ctx.UseMetaData]); err != nil {
		return
	}
	if err = form.AddFile("FILENAME", ctx.SourceFile); err != nil {
		return
	}

	return
}

func importAudio(ctx *Context, res *Result) (err error) {
	ctx.dbglog.Printf("importer: importAudio() called for cart/cut: %d/%d", ctx.Cart, ctx.Cut)
	easy := curl.EasyInit()

	if easy != nil {
		defer easy.Cleanup()

		easy.Setopt(curl.OPT_URL, ctx.conf.RDXportEndpoint)
		easy.Setopt(curl.OPT_POST, true)
		easy.Setopt(curl.OPT_USERAGENT, "Radio Helsinki Import")

		var form *curl.Form
		if form, err = importAudioCreateRequest(ctx, easy); err != nil {
			return
		}
		easy.Setopt(curl.OPT_HTTPPOST, form)
		easy.Setopt(curl.OPT_HTTPHEADER, []string{"Expect:"})

		var resbody bytes.Buffer
		easy.Setopt(curl.OPT_WRITEFUNCTION, func(ptr []byte, userdata interface{}) bool {
			b := userdata.(*bytes.Buffer)
			b.Write(ptr)
			return true
		})
		easy.Setopt(curl.OPT_WRITEDATA, &resbody)

		easy.Setopt(curl.OPT_NOPROGRESS, false)
		easy.Setopt(curl.OPT_PROGRESSFUNCTION, func(dltotal, dlnow, ultotal, ulnow float64, userdata interface{}) bool {
			if ctx.isCanceled() {
				res.ResponseCode = http.StatusNoContent
				res.ErrorString = "canceled"
				return false
			}

			ctx.reportProgress(3, "importing", ulnow, ultotal)
			return true
		})
		easy.Setopt(curl.OPT_PROGRESSDATA, ctx)

		if err = easy.Perform(); err != nil {
			if res.ResponseCode == http.StatusNoContent {
				ctx.stdlog.Printf("import to cart/cat %d/%d got canceled", ctx.Cart, ctx.Cut)
				res.Cart = ctx.Cart
				res.Cut = ctx.Cut
				err = nil
			} else {
				err = fmt.Errorf("importer: %s", err)
			}
			return
		}

		var rdres *rdWebResult
		if rdres, err = newRDWebResultFromXML(bufio.NewReader(&resbody)); err != nil {
			return
		}
		res.fromRDWebResult(rdres)
		res.Cart = ctx.Cart
		res.Cut = ctx.Cut
	} else {
		err = fmt.Errorf("Error initializing libcurl")
	}

	return
}

//****************************************************************
//**   global stuff

func addCartCut(ctx *Context, res *Result) (err error) {
	if err = addCart(ctx, res); err != nil || res.ResponseCode != http.StatusOK {
		return
	}
	if err = addCut(ctx, res); err != nil || res.ResponseCode != http.StatusOK {
		return removeCart(ctx, &Result{ResponseCode: http.StatusOK})
	}
	return
}

func removeAddCartCut(ctx *Context, res *Result) (err error) {
	if err = removeCart(ctx, res); err != nil || (res.ResponseCode != http.StatusOK && res.ResponseCode != http.StatusNotFound) {
		return
	}
	return addCartCut(ctx, res)
}

func clearShowCarts(ctx *Context, res *Result, carts []uint) (err error) {
	if ctx.ClearShowCarts {
		origCart := ctx.Cart
		for _, cart := range carts {
			ctx.Cart = cart
			if err = removeCart(ctx, res); err != nil || (res.ResponseCode != http.StatusOK && res.ResponseCode != http.StatusNotFound) {
				return
			}
		}
		ctx.Cart = origCart
	}
	return
}

func addShowCartCut(ctx *Context, res *Result, carts []uint) (err error) {
	if ctx.Cart != 0 {
		return addCartCut(ctx, res)
	}

	for _, cart := range carts {
		ctx.Cart = cart
		if err = addCart(ctx, res); err != nil {
			return
		}
		switch res.ResponseCode {
		case http.StatusOK:
			{
				if err = addCut(ctx, res); err != nil || res.ResponseCode != http.StatusOK {
					return removeCart(ctx, &Result{ResponseCode: http.StatusOK})
				}
				return
			}
		case http.StatusForbidden: // Cart already exists
			continue
		default:
			return
		}
	}
	res.ResponseCode = http.StatusForbidden
	res.ErrorString = fmt.Sprintf("Show %d has no free carts left", ctx.ShowId)
	return
}

func ImportFile(ctx *Context) (res *Result, err error) {
	res = &Result{ResponseCode: http.StatusOK}

	ctx.stdlog.Printf("ImportFile: called with: show-id: %d, pool-name: '%s', cart/cut: %d/%d", ctx.ShowId, ctx.GroupName, ctx.Cart, ctx.Cut)

	// TODO: on trusted interfaces we should call getPassword again with cached=false after 401's
	if ctx.Trusted {
		if err = ctx.getPassword(true); err != nil {
			return
		}
	}

	rmCartOnErr := false
	rmCutOnErr := false
	if ctx.ShowId != 0 { // Import to a show
		if err = clearShowCarts(ctx, res, ctx.ShowCarts); err != nil || (res.ResponseCode != http.StatusOK && res.ResponseCode != http.StatusNotFound) {
			return
		}
		if ctx.ClearCart && !ctx.ClearShowCarts {
			if err = removeAddCartCut(ctx, res); err != nil || res.ResponseCode != http.StatusOK {
				return
			}
		} else {
			if err = addShowCartCut(ctx, res, ctx.ShowCarts); err != nil || res.ResponseCode != http.StatusOK {
				return
			}
		}
		rmCartOnErr = true
	} else if ctx.GroupName != "" { // Import to music pool
		if err = addCartCut(ctx, res); err != nil || res.ResponseCode != http.StatusOK {
			return
		}
		rmCartOnErr = true
	} else if ctx.Cart != 0 && ctx.Cut == 0 { // Import to Cart
		if ctx.ClearCart {
			if err = removeAddCartCut(ctx, res); err != nil || res.ResponseCode != http.StatusOK {
				return
			}
			rmCartOnErr = true
		} else {
			if err = addCut(ctx, res); err != nil {
				return
			}
			if res.ResponseCode != http.StatusOK {
				if err = addCartCut(ctx, res); err != nil || res.ResponseCode != http.StatusOK {
					return
				}
				rmCartOnErr = true
			} else {
				rmCutOnErr = true
			}
		}
	}

	if ctx.Cart != 0 && ctx.Cut != 0 { // Import to specific Cut within Cart
		if err = importAudio(ctx, res); err != nil || res.ResponseCode != http.StatusOK {
			if err != nil {
				ctx.stdlog.Printf("ImportFile: import failed (Cart/Cut %d/%d): %s", ctx.Cart, ctx.Cut, err)
			} else {
				ctx.stdlog.Printf("ImportFile: import failed (Cart/Cut %d/%d): %s", res.Cart, res.Cut, res.ErrorString)
			}
			// Try to clean up after failed import
			if rmCartOnErr {
				if rerr := removeCart(ctx, &Result{ResponseCode: http.StatusOK}); rerr != nil {
					return
				}
			} else if rmCutOnErr {
				if rerr := removeCut(ctx, &Result{ResponseCode: http.StatusOK}); rerr != nil {
					return
				}
			}
		} else {
			if err := ctx.updateCutCartTitle(); err != nil {
				ctx.stdlog.Printf("Warning: error while updating Cart/Cut Title: %v", err)
			}
			ctx.stdlog.Printf("ImportFile: succesfully imported into Cart/Cut %d/%d", res.Cart, res.Cut)
		}
	} else {
		res.ResponseCode = http.StatusBadRequest
		res.ErrorString = "ImportFile: The request doesn't contain enough information to be processed"
	}

	return
}
