//
//  rhimportd
//
//  The Radio Helsinki Rivendell Import Daemon
//
//
//  Copyright (C) 2015-2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of rhimportd.
//
//  rhimportd is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  rhimportd is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with rhimportd. If not, see <http://www.gnu.org/licenses/>.
//

package rhimport

import (
	"encoding/json"
	"fmt"
	"io"
)

type youtubeDLInfo struct {
	ID           string            `json:"id"`
	Title        string            `json:"title"`
	URL          string            `json:"url"`
	Extractor    string            `json:"extractor"`
	ExtractorKey string            `json:"extractor_key"`
	Ext          string            `json:"ext"`
	Protocol     string            `json:"protocol"`
	HTTPHeaders  map[string]string `json:"http_headers"`
}

func newYoutubeDLInfoFromJSON(data io.Reader) (res *youtubeDLInfo, err error) {
	decoder := json.NewDecoder(data)
	res = &youtubeDLInfo{}
	if jsonerr := decoder.Decode(res); jsonerr != nil {
		err = fmt.Errorf("Error parsing JSON response: %s", jsonerr)
		return
	}
	return
}
