# The Radio Helsinki Rivendell Go Packages

This repository contains a set of golang packages with bindings to the Rivendell
database, tools and utils around Rivendell. They can be used for various tasks in
the Radio Helsinki Deployment of Rivendell.

## Status

This is currently on development. Any api calls are subject to change!

## API

tba...

## Licence

    © 2016 Christian Pointner <equinox@spreadspace.org>    GPL-3+
