//
//  rhrd-go
//
//  The Radio Helsinki Rivendell Go Package
//
//
//  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of rhrd-go.
//
//  rhrd-go is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  rhrd-go is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with rhrd-go. If not, see <http://www.gnu.org/licenses/>.
//

package rddb

import (
	"github.com/vaughan0/go-ini"
)

type config struct {
	configfile string
	dbHost     string
	dbUser     string
	dbPasswd   string
	dbDb       string
}

func getIniValue(file ini.File, section string, key string, dflt string) string {
	value, ok := file.Get(section, key)
	if ok {
		return value
	}
	return dflt
}

func (self *config) readConfigFile() error {
	file, err := ini.LoadFile(self.configfile)
	if err != nil {
		return err
	}

	self.dbHost = getIniValue(file, "mySQL", "Hostname", "localhost")
	self.dbUser = getIniValue(file, "mySQL", "Loginname", "rivendell")
	self.dbPasswd = getIniValue(file, "mySQL", "Password", "letmein")
	self.dbDb = getIniValue(file, "mySQL", "Database", "rivendell")
	return nil
}

func newConfig(configfile string) (conf *config, err error) {
	conf = new(config)
	conf.configfile = configfile
	if err = conf.readConfigFile(); err != nil {
		return
	}
	return
}
