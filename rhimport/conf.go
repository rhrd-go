//
//  rhimportd
//
//  The Radio Helsinki Rivendell Import Daemon
//
//
//  Copyright (C) 2015-2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of rhimportd.
//
//  rhimportd is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  rhimportd is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with rhimportd. If not, see <http://www.gnu.org/licenses/>.
//

package rhimport

import (
	"strconv"

	"github.com/vaughan0/go-ini"
)

type ImportParamDefaults struct {
	Channels           uint
	NormalizationLevel int
	AutotrimLevel      int
	UseMetaData        bool
}

type Config struct {
	ConfigFile      string
	RDXportEndpoint string
	TempDir         string
	LocalFetchDir   string
	SampleRate      uint
	ImportParamDefaults
}

func getIniValue(file ini.File, section string, key string, dflt string) string {
	value, ok := file.Get(section, key)
	if ok {
		return value
	}
	return dflt
}

func (c *Config) readConfigFile() error {
	file, err := ini.LoadFile(c.ConfigFile)
	if err != nil {
		return err
	}

	sr := getIniValue(file, "Format", "SampleRate", "44100")
	if sr64, err := strconv.ParseUint(sr, 10, 32); err != nil {
		return err
	} else {
		c.SampleRate = uint(sr64)
	}

	ch := getIniValue(file, "Format", "Channels", "2")
	if ch64, err := strconv.ParseUint(ch, 10, 32); err != nil {
		return err
	} else {
		c.ImportParamDefaults.Channels = uint(ch64)
	}

	return nil
}

func NewConfig(configfile, rdxportEndpoint, tempDir, localFetchDir string) (conf *Config, err error) {
	conf = &Config{}
	conf.ConfigFile = configfile
	conf.RDXportEndpoint = rdxportEndpoint
	conf.TempDir = tempDir
	conf.LocalFetchDir = localFetchDir
	conf.SampleRate = 44100
	conf.ImportParamDefaults.Channels = 2
	conf.ImportParamDefaults.NormalizationLevel = 0
	conf.ImportParamDefaults.AutotrimLevel = 0
	conf.ImportParamDefaults.UseMetaData = true

	if err = conf.readConfigFile(); err != nil {
		return
	}
	return
}
